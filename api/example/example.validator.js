import { AbstractValidator } from '../helpers/AbstractValidator';
import Validator from 'node-validator';

export class ExampleValidator extends AbstractValidator {
  constructor() {
    super();
  }

  get createExample() {
    return this._createExample.bind(this)();
  }

  _createExample() {
    const createExampleRules = Validator.isObject()
      .withRequired('textField', Validator.isString())
      .withRequired('numberField', Validator.isNumber());

    return this.runMiddleware.bind(this, createExampleRules, 'body');
  }
}
