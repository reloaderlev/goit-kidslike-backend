import { NotFoundError } from '../helpers/errorsConstructors';
import { OK, CREATED } from 'http-status-codes';
import { Example } from './example.dao';
import { exampleDto } from './example.dto';

export class ExampleController {
  get getExample() {
    return this._getExample.bind(this);
  }
  get createExample() {
    return this._createExample.bind(this);
  }

  async _getExample(req, res) {
    const example = await Example.findOne();
    if (!example) {
      throw new NotFoundError('There is no examples yet');
    }

    return res.status(OK).json(exampleDto.composeExampleResponse(example));
  }

  async _createExample(req, res) {
    const { textField, numberField } = req.body;
    const example = await Example.createExample(textField, numberField);

    return res.status(CREATED).json(exampleDto.composeExampleResponse(example));
  }
}
