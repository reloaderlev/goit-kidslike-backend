// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { KidslikeServer } = require('../server');
const request = require('supertest-promised');
const { NOT_FOUND, OK } = require('http-status-codes');
const { Example } = require('./example.dao');

describe('Example API', () => {
  let server;
  let kidslikeServer;

  before(async () => {
    kidslikeServer = new KidslikeServer();
    await kidslikeServer.start();
    await kidslikeServer.truncateModels(Example);
    server = kidslikeServer.server;
  });

  after(() => {
    server.close();
  });

  describe('GET /', () => {
    context('when there is no examples', () => {
      it('should throw 404 error', async () => {
        await request(server)
          .get('/api/examples')
          .expect(NOT_FOUND)
          .end();
      });
    });

    context('when there is at least one example', () => {
      const expectedExample = {
        textField: 'someTextField',
        numberField: 4,
      };

      beforeEach(async () => {
        await Example.create(expectedExample);
      });

      afterEach(async () => {
        await kidslikeServer.truncateModels(Example);
      });

      it('should return successful response', async () => {
        const responseBody = await request(server)
          .get('/api/examples')
          .expect(OK)
          .end()
          .get('body');

        responseBody.example.should.be.containEql(expectedExample);
      });
    });
  });
});
