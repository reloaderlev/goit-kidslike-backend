export class ExampleDto {
  composeExample(entity) {
    const { id, textField, numberField } = entity;
    return {
      id,
      textField,
      numberField,
    };
  }

  composeExampleResponse(entity) {
    const example = this.composeExample(entity);
    return {
      example,
    };
  }
}

export const exampleDto = new ExampleDto();
