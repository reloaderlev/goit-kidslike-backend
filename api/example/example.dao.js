import { Model } from 'sequelize';

export class Example extends Model {
  static createExample(textField, numberField) {
    return this.create({ textField, numberField });
  }
}
