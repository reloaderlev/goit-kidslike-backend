import { STRING, INTEGER } from 'sequelize';
import { Example } from './example.dao';

export const initExample = Example.init.bind(Example, {
  id: { type: INTEGER, primaryKey: true, autoIncrement: true },
  textField: { type: STRING, allowNull: false },
  numberField: { type: INTEGER, allowNull: false },
});
