import { ExampleController } from './example.controller';
import { ExampleValidator } from './example.validator';
import { asyncWrapper } from '../helpers/asyncMiddlewareWrapper';
import { Router } from 'express';

export class ExampleRouter {
  static initRouter() {
    const exampleController = new ExampleController();
    const exampleValidator = new ExampleValidator();
    const router = Router();

    router.get('/', asyncWrapper(exampleController.getExample));
    router.post(
      '/',
      exampleValidator.createExample,
      asyncWrapper(exampleController.createExample),
    );

    return router;
  }
}

export const exampleRouter = ExampleRouter.initRouter();
