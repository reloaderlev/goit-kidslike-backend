import Validator from 'node-validator';
import { AbstractValidator } from '../helpers/AbstractValidator';

export class TaskValidator extends AbstractValidator {
  constructor() {
    super();
  }

  get task() {
    return this._task.bind(this)();
  }

  _task() {
    const taskRules = Validator.isObject()
      .withRequired('title', Validator.isString())
      .withRequired('taskPoints', Validator.isNumber({ min: 1, max: 9 }));

    return this.runMiddleware.bind(this, taskRules, 'body');
  }
}
