import { Router } from 'express';
import { authMiddleware } from '../helpers/auth.middleware';
import { TaskValidator } from './task.validator';
import { asyncWrapper } from '../helpers/asyncMiddlewareWrapper';
import { TaskController } from './task.controller';

class TaskRouter {
  static initRouter() {
    const taskValidator = new TaskValidator();
    const taskController = new TaskController();
    const router = Router();

    router.post('/', authMiddleware, taskValidator.task, asyncWrapper(taskController.createCustomTask));

    return router;
  }
}

export const taskRouter = TaskRouter.initRouter();
