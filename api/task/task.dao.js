import { Model, Op } from 'sequelize';
import { ScheduledTask } from '../scheduledTask/scheduledTask.dao';

export class Task extends Model {
  static createTask(title, points, imagePath, authorId) {
    return this.create({ title, points, imagePath, authorId });
  }

  static findTaskById(id) {
    return this.findByPk(id);
  }

  static getAllTasks() {
    return this.findAll();
  }

  static findTaskByName(title) {
    return this.findOne({ where: { title } });
  }

  static findUserTaskByName(title, authorId) {
    return this.findOne({ where: { title, authorId } });
  }

  static initDefaultTask(title, points, imagePath, authorId) {
    return this.findOrCreate({ where: { title, points, imagePath, authorId } });
  }

  static findUserTasksWithSchedule(userId, dates) {
    return this.findAll({
      where: { [Op.or]: [{ authorId: null }, { authorId: userId }] },
      include: [
        {
          model: ScheduledTask,
          as: 'schedule',
          where: {
            ownerId: userId,
            dueDate: dates,
          },
          required: false,
        },
      ],
    });
  }
}
