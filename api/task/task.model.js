import { STRING, SMALLINT, INTEGER } from 'sequelize';
import { Task } from './task.dao.js';

export const initTask = Task.init.bind(Task, {
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: STRING,
    allowNull: false,
  },
  points: {
    type: SMALLINT,
    allowNull: false,
  },
  imagePath: {
    type: STRING,
  },
  authorId: {
    type: INTEGER,
  },
});
