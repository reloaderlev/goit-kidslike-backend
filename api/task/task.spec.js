// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { KidslikeServer } = require('../server');
const request = require('supertest-promised');
const { BAD_REQUEST, UNAUTHORIZED, CONFLICT, CREATED } = require('http-status-codes');
const { User } = require('../user/user.dao');
const { Session } = require('../session/session.dao');
const { ScheduledTask } = require('../scheduledTask/scheduledTask.dao');
const { Task } = require('../task/task.dao');
const { AuthController } = require('../auth/auth.controller');
const { taskDto } = require('./task.dto');

describe('Task API', () => {
  let server;
  let kidslikeServer;
  let user;
  let token;
  const task = {
    title: 'some validtask',
    taskPoints: 5,
  };
  const authController = new AuthController();
  const firstUser = {
    email: 'taskUser@user.ua',
    password: 'taskPassword',
  };

  before(async () => {
    kidslikeServer = new KidslikeServer();
    await kidslikeServer.start();
    await kidslikeServer.truncateModels(User, Session, Task, ScheduledTask);
    server = kidslikeServer.server;
    const { email, password } = firstUser;
    const [{ dataValues: UserInstance }] = await User.createUser(email, password);
    user = UserInstance;
    token = await authController.createSessionToken(user.id);
  });

  after(async () => {
    await kidslikeServer.truncateModels(User, Session, Task, ScheduledTask);
    server.close();
  });

  describe('POST /api/tasks', () => {

    afterEach(async () => {
      await kidslikeServer.truncateModels(Task, ScheduledTask);
    });

    context('when use invalid token', () => {
      it('should throw 401 error', async () => {
        await request(server)
          .post('/api/tasks')
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when request body format is invalid', () => {

      it('should throw 400 error before request body is invalid', async () => {
        const invalidTask = {
          tutle: 'some task text',
          taskPoints: 11,
        };

        await request(server)
          .post('/api/tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send(invalidTask)
          .expect(BAD_REQUEST)
          .end();
      });
    });

    context('when task with such name already exist', () => {

      it('should throw 409 error', async () => {
        const { title, taskPoints } = task;

        await Task.createTask(title, taskPoints, 'some task image', user.id);
        await request(server)
          .post('/api/tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send(task)
          .expect(CONFLICT)
          .end();
      });
    });

    context('when task created', () => {

      it('should be status CREATED(201)', async () => {

        const response = await request(server)
          .post('/api/tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send(task)
          .expect(CREATED)
          .end()
          .get('body');

        const { dataValues: createdTask } = await Task.findUserTaskByName(task.title, user.id);
        const { id, title, points, imagePath } = createdTask;

        response.should.have.property('tasks').which.is.an.Array()
          .and.have.matchEach(function (scheduleTask) {
            scheduleTask.should.have.value('id', id);
            scheduleTask.should.have.value('title', title);
            scheduleTask.should.have.value('imagePath', taskDto.composeImageUrl(imagePath));
            scheduleTask.should.have.value('points', points);
            scheduleTask.should.have.property('schedule')
              .which.is.Array().and.have.matchEach(function (day) {
                day.should.have.value('status', 'cancelled');
                day.should.have.property('scheduleTaskId');
                day.should.have.property('dueDate');
              });
          });
      });
    });
  });
});
