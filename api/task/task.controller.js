import { CREATED } from 'http-status-codes';
import { Task } from './task.dao';
import { taskDto } from './task.dto';
import { ScheduledTaskController } from '../scheduledTask/scheduledTask.controller';
import { ConflictError } from '../helpers/errorsConstructors';

export class TaskController {
  get createCustomTask() {
    return this._createCustomTask.bind(this);
  }

  async _createCustomTask(req, res) {
    const { title: taskTitle, taskPoints } = req.body;
    const { user } = req.userSession;
    const isTaskExist = await Task.findUserTaskByName(taskTitle, user.id);

    if (isTaskExist) throw new ConflictError(`Task with title "${taskTitle}" already exist!`);

    const { dataValues: createdTask } = await Task.createTask(taskTitle, taskPoints, '', user.id);
    const scheduledTasks = await ScheduledTaskController.createScheduledTasks(createdTask, user);
    const responseBody = taskDto.composeTaskWithScheduleResponse(createdTask, scheduledTasks);

    return res
      .status(CREATED)
      .send(responseBody)
      .end();
  }
}
