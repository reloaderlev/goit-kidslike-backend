import config from '../config';
import { scheduledTaskDto } from '../scheduledTask/scheduledTask.dto';

class TaskDto {
  composeTaskWithScheduleResponse(task, scheduledTasks) {
    return {
      tasks: [this.composeTaskWithSchedule(task, scheduledTasks)],
    };
  }

  composeTaskWithSchedule(task, scheduledTasks) {
    const schedule = scheduledTasks.map(scheduledTaskDto.composeScheduleTask);

    return {
      id: task.id,
      schedule: schedule,
      title: task.title,
      imagePath: this.composeImageUrl(task.imagePath),
      points: task.points,
    };
  }

  composeImageUrl(imageFilename) {
    if (!imageFilename) {
      return '';
    }
    return `${config.staticBasePath}/${imageFilename}`;
  }
}

export const taskDto = new TaskDto();
