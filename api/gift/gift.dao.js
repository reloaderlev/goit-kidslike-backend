import { Model } from 'sequelize';

export class Gift extends Model {
  static createGift(title, cost, imagePath) {
    return this.create({ title, cost, imagePath });
  }

  static getAllGifts() {
    return this.findAll({ raw: true });
  }

  static findGiftById(id) {
    return this.findByPk(id);
  }
}
