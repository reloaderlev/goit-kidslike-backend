import { OK, CREATED } from 'http-status-codes';
import { Gift } from './gift.dao';
import { giftDto } from './gift.dto';
import { NotFoundError, ForbiddenError } from '../helpers/errorsConstructors';
import { User } from '../user/user.dao';
import { userDto } from '../user/user.dto';

export class GiftController {
  get getAllGifts() {
    return this._getAllGifts.bind(this);
  }

  get purchaseGift() {
    return this._purchaseGift.bind(this);
  }

  async _getAllGifts(req, res) {

    const gifts = await Gift.getAllGifts();
    const responseBody = giftDto.composeAllGiftsResponse(gifts);

    return res
      .status(OK)
      .send(responseBody)
      .end();
  }

  async _purchaseGift(req, res) {

    const { userSession: { user }, params: { id } } = req;
    const giftInstance = await Gift.findGiftById(id);

    if (!giftInstance) throw new NotFoundError(`Gift with id:${id} does not exist!`);

    const { dataValues: { cost } } = giftInstance;

    if (cost > user.balance) throw new ForbiddenError('User balance is too small for gift purchase!');

    const [, [{ dataValues: updatedUser }]] = await User.changeUserBalance(-cost, user.id);

    const responseBody = userDto.composeUserBalance(updatedUser);

    return res
      .status(CREATED)
      .send(responseBody)
      .end();
  }
}
