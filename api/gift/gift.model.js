import { INTEGER, STRING } from 'sequelize';
import { Gift } from './gift.dao';

export const initGift = Gift.init.bind(Gift, {
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: STRING,
    allowNull: false,
  },
  cost: {
    type: INTEGER,
    allowNull: false,
  },
  imagePath: {
    type: STRING,
    allowNull: false,
  },
});
