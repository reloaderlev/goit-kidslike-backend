class GiftDto {
  composeAllGiftsResponse(allGifts) {
    const gifts = allGifts.map(({ id, title, cost, imagePath }) => ({ id, title, cost, imagePath }));
    return { gifts };
  }
}

export const giftDto = new GiftDto();
