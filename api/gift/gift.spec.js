// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { KidslikeServer } = require('../server');
const request = require('supertest-promised');
const { UNAUTHORIZED, OK, CREATED, FORBIDDEN, NOT_FOUND } = require('http-status-codes');
const { AuthController } = require('../auth/auth.controller');
const { User } = require('../user/user.dao');
const { Session } = require('../session/session.dao');
const { Gift } = require('../gift/gift.dao');

describe('Gift API', () => {
  let server;
  let kidslikeServer;
  let token;
  let createdGift;
  let createdUser;
  const authController = new AuthController();
  const user = {
    email: 'giftUser@user.ua',
    password: 'giftPassword',
  };
  const gift = {
    title: 'some gift',
    cost: 5,
    imagePath: 'gift image',
  };

  before(async () => {
    kidslikeServer = new KidslikeServer();
    await kidslikeServer.start();
    await kidslikeServer.truncateModels(User, Session, Gift);
    server = kidslikeServer.server;
    const { email, password } = user;
    const { title, cost, imagePath } = gift;
    const [{ dataValues: userInstance }] = await User.createUser(email, password);
    createdUser = userInstance;
    token = await authController.createSessionToken(userInstance.id);
    const { dataValues } = await Gift.createGift(title, cost, imagePath);
    createdGift = dataValues;
  });

  after(async () => {
    await kidslikeServer.truncateModels(User, Session, Gift);
    server.close();
  });

  describe('GET /api/gift', () => {

    context('when use invalid token', () => {

      it('should throw 401 error', async () => {

        await request(server)
          .get('/api/gift')
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when response with gifts', () => {

      it('should be status 200(OK) and gifts array', async () => {

        const { id, title, cost, imagePath } = createdGift;

        const response = await request(server)
          .get('/api/gift')
          .set('Authorization', `Bearer ${token}`)
          .expect(OK)
          .end()
          .get('body');

        response.should.have.property('gifts')
          .which.is.an.Array()
          .and.have.matchEach(function (giftInstanse) {
            giftInstanse.should.have.value('id', id);
            giftInstanse.should.have.value('title', title);
            giftInstanse.should.have.value('cost', cost);
            giftInstanse.should.have.value('imagePath', imagePath);
          });
      });
    });
  });

  describe('POST /api/gift/id', () => {

    context('when use invalid token', () => {

      it('should throw 401 error', async () => {

        await request(server)
          .post(`/api/gift/${createdGift.id}`)
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when gift with such id does not exist', () => {

      it('should throw 404 error', async () => {

        await request(server)
          .post('/api/gift/0')
          .set('Authorization', `Bearer ${token}`)
          .expect(NOT_FOUND)
          .end();
      });
    });

    context('when user balance is too small for gift purchase', () => {

      it('should throw 403 error', async () => {

        await request(server)
          .post(`/api/gift/${createdGift.id}`)
          .set('Authorization', `Bearer ${token}`)
          .expect(FORBIDDEN)
          .end();
      });
    });

    context('when user balance is too small for gift purchase', () => {

      before(async () => {
        await User.changeUserBalance(createdGift.cost, createdUser.id);
      });

      it('should be status 201(CREATED) and returned user balance', async () => {

        const response = await request(server)
          .post(`/api/gift/${createdGift.id}`)
          .set('Authorization', `Bearer ${token}`)
          .expect(CREATED)
          .end()
          .get('body');

        const { dataValues: changedUser } = await User.findUserById(createdUser.id);
        const { balance: changedBalance } = changedUser;

        changedBalance.should.be.equal(0);
        response.should.be.an.Object().and.have.value('balance', changedBalance);
	
      });
    });
  });
});
