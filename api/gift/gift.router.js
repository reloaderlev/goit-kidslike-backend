import { Router } from 'express';
import { GiftController } from './gift.controller';
import { asyncWrapper } from '../helpers/asyncMiddlewareWrapper';
import { authMiddleware } from '../helpers/auth.middleware';

class GiftRouter {
  static initRouter() {
    const giftController = new GiftController();
    const router = Router();

    router.get('/', authMiddleware, asyncWrapper(giftController.getAllGifts));
    router.post('/:id', authMiddleware, asyncWrapper(giftController.purchaseGift));

    return router;
  }
}

export const giftRouter = GiftRouter.initRouter();
