import { Model } from 'sequelize';

export const scheduledTaskStatus = {
  SCHEDULED: 'scheduled',
  NOTSCHEDULED: 'cancelled',
  DONE: 'done',
};

export class ScheduledTask extends Model {
  static createScheduledTask(taskId, ownerId, authorId, title, points, imagePath, dueDate, status = scheduledTaskStatus.NOTSCHEDULED) {
    return this.create({
      taskId,
      ownerId,
      authorId,
      title,
      points,
      imagePath,
      dueDate,
      status,
    });
  }

  static getScheduleTask(taskId, ownerId, dueDate) {
    return this.findAll({
      where: {
        taskId,
        ownerId,
        dueDate,
      },
    });
  }

  static updateScheduleTask(id, status) {
    return this.update({ status }, { where: { id }, returning: true });
  }

  static findUserScheduledTask(id, ownerId) {
    return this.findOne({ where: { id, ownerId } });
  }

  static findAllUsersScheduledTasks(ownerId, dates) {
    return this.findAll({
      where: {
        ownerId,
        dueDate: dates,
      },
      raw: true,
    });
  }

  static findScheduleTask(id) {
    return this.findOne({ where: { id }, raw: true });
  }
}
