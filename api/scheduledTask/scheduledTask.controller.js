import { OK } from 'http-status-codes';
import { ForbiddenError, NotFoundError } from '../helpers/errorsConstructors';
import { scheduledTaskStatus, ScheduledTask } from './scheduledTask.dao';
import { Task } from '../task/task.dao';
import { scheduledTaskDto } from './scheduledTask.dto';
import { taskDto } from '../task/task.dto';
import { dateInterval, setActualDates } from '../helpers/dateHelper';
import { UserController } from '../user/user.controller';

export class ScheduledTaskController {

  static async createScheduledTasks(task, user) {

    const days = setActualDates(...dateInterval.currentWeek);
    const scheduledTasks = [];

    for (let date of days) {
      const scheduledTaskArguments = scheduledTaskDto.composeDataForScheduleTask(user, task, date);
      const { dataValues: scheduledTask } = await ScheduledTask.createScheduledTask(...scheduledTaskArguments);
      scheduledTasks.push(scheduledTask);
    }

    return scheduledTasks;
  }

  get scheduledTask() {
    return this._scheduledTask.bind(this);
  }

  get toggledScheduledTask() {
    return this._toggledScheduledTask.bind(this);
  }

  get userScheduledTasks() {
    return this._userScheduledTasks.bind(this);
  }

  async _scheduledTask(req, res) {

    const { body: schudeleTask, userSession: { user } } = req;
    const { taskId, days } = schudeleTask;
    const task = await Task.findTaskById(taskId);

    if (!task) throw new NotFoundError(`Task with id ${taskId} not found`);

    const { dataValues: actualTask } = task;
    const { authorId } = actualTask;

    if (authorId && user.id !== authorId) throw new ForbiddenError(`User with id ${user.id} cannot schedule this task`);

    const updatedScheduledTasks = await this._updateScheduleTasks(taskId, user.id, days);

    if (!updatedScheduledTasks.length) throw new Error();

    const responseBody = taskDto.composeTaskWithScheduleResponse(task, updatedScheduledTasks);

    return res
      .status(OK)
      .send(responseBody)
      .end();
  }

  async _updateScheduleTasks(taskId, userId, days) {
    const updatedScheduledTasks = [];

    for (let day of days) {
      const { date, schedule } = day;
      const [{ dataValues: scheduleTask }] = await ScheduledTask.getScheduleTask(taskId, userId, date);
      const { id } = scheduleTask;
      const status = schedule ? scheduledTaskStatus.SCHEDULED : scheduledTaskStatus.NOTSCHEDULED;
      const [, [{ dataValues: scheduledTask }]] = await ScheduledTask.updateScheduleTask(id, status);

      updatedScheduledTasks.push(scheduledTask);
    }
    return updatedScheduledTasks;
  }

  async _toggledScheduledTask(req, res) {
    const toggledScheduledTasks = req.body;
    const { user } = req.userSession;

    for (let toggledScheduledTask of toggledScheduledTasks) {
      const { scheduleTaskId } = toggledScheduledTask;
      const userIsOwner = await ScheduledTask.findUserScheduledTask(scheduleTaskId, user.id);

      if (!userIsOwner) throw new ForbiddenError(`User with id ${user.id} cannot schedule this task`);
    }

    const updatedToggledScheduledTasks = await this._updateToggledScheduledTasks(toggledScheduledTasks);
    await UserController.changeUserBalance(updatedToggledScheduledTasks);
    const responseBody = scheduledTaskDto.composeToggledScheduledTask(updatedToggledScheduledTasks);

    return res
      .status(OK)
      .send(responseBody)
      .end();
  }

  async _updateToggledScheduledTasks(toggledScheduledTasks) {
    const updatedToggledScheduledTasks = [];

    for (let toggledScheduledTask of toggledScheduledTasks) {
      const { scheduleTaskId, status } = toggledScheduledTask;
      const { status: previousStatus } = await ScheduledTask.findScheduleTask(scheduleTaskId);
      const [, [{ dataValues: updatedToggleScheduledTask }]] = await ScheduledTask.updateScheduleTask(scheduleTaskId, status);

      updatedToggledScheduledTasks.push({ ...updatedToggleScheduledTask, previousStatus });
    }

    return updatedToggledScheduledTasks;
  }

  async _userScheduledTasks(req, res) {
    const { user } = req.userSession;
    const currentDates = setActualDates(...dateInterval.currentWeek);
    const allUsersScheduledTasks = await Task.findUserTasksWithSchedule(user.id, currentDates);

    const responseBody = scheduledTaskDto.composeAllUsersScheduledTasks(allUsersScheduledTasks);

    return res
      .status(OK)
      .send(responseBody)
      .end();
  }
}
