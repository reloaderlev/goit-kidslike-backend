import { Router } from 'express';
import { ScheduledTaskValidator } from './scheduledTask.validator';
import { ScheduledTaskController } from './scheduledTask.controller';
import { asyncWrapper } from '../helpers/asyncMiddlewareWrapper';
import { authMiddleware } from '../helpers/auth.middleware';

class ScheduledTaskRouter {
  static initRouter() {
    const scheduledTaskValidator = new ScheduledTaskValidator();
    const scheduledTaskController = new ScheduledTaskController();
    const router = Router();

    router.get('/', authMiddleware, asyncWrapper(scheduledTaskController.userScheduledTasks));
    router.post('/', authMiddleware, scheduledTaskValidator.scheduledTask, asyncWrapper(scheduledTaskController.scheduledTask));
    router.patch('/', authMiddleware, scheduledTaskValidator.toggledScheduledTask, asyncWrapper(scheduledTaskController.toggledScheduledTask));

    return router;
  }
}

export const scheduledTaskRouter = ScheduledTaskRouter.initRouter();
