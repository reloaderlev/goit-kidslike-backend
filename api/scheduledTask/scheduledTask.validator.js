import Validator from 'node-validator';
import { AbstractValidator } from '../helpers/AbstractValidator';
import { ValidationError } from '../helpers/errorsConstructors';
import { isDayOfCurrentWeek } from '../helpers/dateHelper';
import { scheduledTaskStatus } from './scheduledTask.dao';

export class ScheduledTaskValidator extends AbstractValidator {
  constructor() {
    super();
  }
  get scheduledTask() {
    return this._scheduledTask.bind(this)();
  }

  get toggledScheduledTask() {
    return this._toggledScheduledTask.bind(this)();
  }

  _scheduledTask() {

    function сustomDateValidator({ date }) {
      const isCurrentWeek = isDayOfCurrentWeek(date);

      if (!isCurrentWeek) throw new ValidationError('Invalid date value');
    }

    const scheduledTaskRules = Validator.isObject()
      .withRequired('taskId', Validator.isNumber())
      .withRequired('days', Validator.isArray(
        Validator.isObject()
          .withRequired('date', Validator.isDate()).withCustom(сustomDateValidator)
          .withRequired('schedule', Validator.isBoolean()), { min: 1 }));

    return this.runMiddleware.bind(this, scheduledTaskRules, 'body');
  }

  _toggledScheduledTask() {

    const { SCHEDULED, DONE } = scheduledTaskStatus;

    function customStatusValidator({ status }) {
      if (status === SCHEDULED || status === DONE) return;
      throw new ValidationError('Invalid status');
    }

    const toggledScheduledTaskRules = Validator.isArray(Validator.isObject()
      .withRequired('scheduleTaskId', Validator.isNumber({ min: 1 }))
      .withRequired('status', Validator.isString()).withCustom(customStatusValidator), { min: 1 });

    return this.runMiddleware.bind(this, toggledScheduledTaskRules, 'body');
  }
}
