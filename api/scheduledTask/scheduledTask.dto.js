import moment from 'moment';
import { taskDto } from '../task/task.dto';

class ScheduledTaskDto {
  get composeScheduleTask() {
    return this._composeScheduleTask.bind(this);
  }
  get composeToggledScheduledTask() {
    return this._composeToggledScheduledTask.bind(this);
  }

  _composeToggledScheduledTask(updatedToggledScheduledTasks) {
    const tasks = [];

    for (let updatedToggledScheduledTask of updatedToggledScheduledTasks) {
      const {
        id: scheduleTaskId,
        taskId: id,
        title,
        imagePath,
        points,
        dueDate,
        status,
      } = updatedToggledScheduledTask;

      tasks.push({
        id,
        schedule: [{ scheduleTaskId, dueDate: this.composeDate(dueDate), status }],
        title,
        imagePath,
        points,
      });
    }

    return { tasks };
  }

  composeAllUsersScheduledTasks(userTasksWithSchedule) {
    return {
      tasks: userTasksWithSchedule.map((task) => {
        return taskDto.composeTaskWithSchedule(task, task.schedule);
      }),
    };
  }

  composeDataForScheduleTask(user, task, date, status) {
    const { id: ownerId } = user;
    const { id, authorId, title, points, imagePath } = task;

    return [id, ownerId, authorId, title, points, imagePath, date, status];
  }

  _composeScheduleTask(scheduleTask) {
    return {
      scheduleTaskId: scheduleTask.id,
      dueDate: this.composeDate(scheduleTask.dueDate),
      status: scheduleTask.status,
      points: scheduleTask.points,
    };
  }

  composeDate(date) {
    return moment(date).format('YYYY-MM-DD');
  }
}

export const scheduledTaskDto = new ScheduledTaskDto();
