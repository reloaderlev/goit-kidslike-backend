import { INTEGER, STRING, SMALLINT, DATE } from 'sequelize';
import { ScheduledTask, scheduledTaskStatus } from './scheduledTask.dao';

export const initScheduledTask = ScheduledTask.init.bind(ScheduledTask, {
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  taskId: {
    type: INTEGER,
    allowNull: false,
  },
  ownerId: {
    type: INTEGER,
    allowNull: false,
  },
  authorId: {
    type: INTEGER,
  },
  title: {
    type: STRING,
    allowNull: false,
  },
  points: {
    type: SMALLINT,
    allowNull: false,
  },
  imagePath: {
    type: STRING,
  },
  status: {
    type: STRING,
    enum: [scheduledTaskStatus.SCHEDULED, scheduledTaskStatus.DONE, scheduledTaskStatus.NOTSCHEDULED],
    allowNull: false,
  },
  dueDate: {
    type: DATE,
    allowNull: false,
  },
});
