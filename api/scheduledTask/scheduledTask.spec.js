// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { KidslikeServer } = require('../server');
const request = require('supertest-promised');
const { BAD_REQUEST, UNAUTHORIZED, FORBIDDEN, NOT_FOUND, OK } = require('http-status-codes');
const { User } = require('../user/user.dao');
const { Session } = require('../session/session.dao');
const { ScheduledTask } = require('../scheduledTask/scheduledTask.dao');
const { Task } = require('../task/task.dao');
const { AuthController } = require('../auth/auth.controller');
const { fullDate } = require('../helpers/dateHelper');
const { taskDto } = require('../task/task.dto');

describe('ScheduledTask API', () => {
  let server;
  let kidslikeServer;
  let user;
  let token;
  let anotherToken;
  let task;
  let createdScheduledTask;
  const authController = new AuthController();
  const firstUser = {
    email: 'scheduledUser@user.ua',
    password: 'scheduledPassword',
  };
  const secondUser = {
    email: 'scheduledUser@user.com.ua',
    password: 'scheduledPassword',
  };
  const validScheduledTask = {
    taskId: 1,
    days: [{ date: fullDate(), schedule: true }],
  };

  before(async () => {
    kidslikeServer = new KidslikeServer();
    await kidslikeServer.start();
    await kidslikeServer.truncateModels(User, Session, ScheduledTask, Task);
    server = kidslikeServer.server;
    const { email, password } = firstUser;
    const [{ dataValues: firstUserInstance }] = await User.createUser(email, password);
    user = firstUserInstance;
    const { email: anotherEmail, password: anotherPassword } = secondUser;
    token = await authController.createSessionToken(firstUserInstance.id);
    const [{ dataValues: secondUserInstance }] = await User.createUser(anotherEmail, anotherPassword);
    anotherToken = await authController.createSessionToken(secondUserInstance.id);
    await Task.createTask('застелити ліжко', 9, 'some image', firstUserInstance.id);
  });

  after(async () => {
    await kidslikeServer.truncateModels(User, Session, ScheduledTask, Task);
    server.close();
  });

  describe('POST /api/scheduled-tasks', () => {

    context('when use invalid token', () => {
      it('should throw 401 error', async () => {
        await request(server)
          .post('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when request body format is invalid', () => {

      it('should throw 400 error invalid type taskId', async () => {
        const invalidScheduledTask = {
          taskId: '1',
          days: [{ date: fullDate(), schedule: true }],
        };

        await request(server)
          .post('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send(invalidScheduledTask)
          .expect(BAD_REQUEST)
          .end();
      });

      it('should throw 400 error invalid format days', async () => {
        const invalidScheduledTask = {
          taskId: 1,
          days: [],
        };

        await request(server)
          .post('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send(invalidScheduledTask)
          .expect(BAD_REQUEST)
          .end();
      });

      it('should throw 404 error because task id not found', async () => {

        const { taskId } = validScheduledTask;

        await request(server)
          .post('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send({ ...validScheduledTask, taskId: taskId - 1 })
          .expect(NOT_FOUND)
          .end();
      });
    });

    context('when user trying to schedule task which is not general and he is not author of it', () => {

      it('should throw 403 error', async () => {

        const { dataValues: { id } } = await Task.findTaskByName('застелити ліжко');

        await request(server)
          .post('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${anotherToken}`)
          .set('Accept', 'application/json')
          .send({ ...validScheduledTask, taskId: id })
          .expect(FORBIDDEN)
          .end();
      });
    });

    context('when scheduled tasks was updated', () => {

      before(async () => {
        const { dataValues: createdTask } = await Task.findTaskByName('застелити ліжко');
        task = createdTask;
        const { id: taskId, title, points, imagePath } = task;
        const { id: userId } = user;
        const { dataValues } = await ScheduledTask.createScheduledTask(taskId, userId, null, title, points, imagePath, fullDate());
        createdScheduledTask = dataValues;
      });

      it('should be statuss OK(200)', async () => {
        const { id: scheduleTaskId, taskId, title, points, imagePath } = createdScheduledTask;
        const scheduledTask = await request(server)
          .post('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send({ ...validScheduledTask, taskId: task.id })
          .expect(OK)
          .end()
          .get('body');

        scheduledTask.should.have.property('tasks')
          .which.is.an.Array()
          .and.have.matchEach(function (scheduleTask) {
            scheduleTask.should.have.value('id', taskId);
            scheduleTask.should.have.value('title', title);
            scheduleTask.should.have.value('imagePath', taskDto.composeImageUrl(imagePath));
            scheduleTask.should.have.value('points', points);
            scheduleTask.should.have.property('schedule')
              .which.is.Array().and.have.matchEach(function (day) {
                day.should.have.value('status', 'scheduled');
                day.should.have.value('scheduleTaskId', scheduleTaskId);
              });
          });
      });
    });
  });

  describe('PATCH /api/scheduled-tasks', () => {
    const toggledScheduledTask = {
      scheduleTaskId: 1,
      status: 'done',
    };

    context('when use invalid token', () => {

      it('should throw 401 error', async () => {

        await request(server)
          .patch('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when request body format is invalid', () => {

      it('should throw 400 error invalid scheduleTaskId', async () => {

        await request(server)
          .patch('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send([{ ...toggledScheduledTask, scheduleTaskId: 0 }])
          .expect(BAD_REQUEST)
          .end();
      });

      it('should throw 400 error invalid status', async () => {

        await request(server)
          .patch('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send([{ ...toggledScheduledTask, status: 'active' }])
          .expect(BAD_REQUEST)
          .end();
      });
    });

    context('when user try to toggle task and he is not owner of it', () => {

      it('should throw 403 error', async () => {

        const { id } = createdScheduledTask;

        await request(server)
          .patch('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${anotherToken}`)
          .set('Accept', 'application/json')
          .send([{ ...toggledScheduledTask, scheduleTaskId: id }])
          .expect(FORBIDDEN)
          .end();
      });
    });

    context('when scheduled task toggled', () => {

      it('should be status 200(OK) and status = done', async () => {

        const { id: scheduleTaskId, taskId: id, title, points, imagePath } = createdScheduledTask;

        const toggledTask = await request(server)
          .patch('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .set('Accept', 'application/json')
          .send([{ ...toggledScheduledTask, scheduleTaskId }])
          .expect(OK)
          .end()
          .get('body');

        toggledTask.should.have.property('tasks')
          .which.is.an.Array()
          .and.have.matchEach(function (response) {
            response.should.have.value('id', id);
            response.should.have.value('title', title);
            response.should.have.value('imagePath', imagePath);
            response.should.have.value('points', points);
            response.should.have.property('schedule')
              .which.is.Array().and.have.matchEach(function (day) {
                day.should.have.value('status', 'done');
                day.should.have.value('scheduleTaskId', scheduleTaskId);

              });
          });
      });
    });
  });

  describe('GET /api/scheduled-tasks', () => {

    context('when use invalid token', () => {

      it('should throw 401 error', async () => {
        await request(server)
          .get('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when use valid token', () => {

      it('should be status 200(OK) and array all of scheduled tasks for user', async () => {

        const { id: scheduleTaskId, taskId: id, title, points, imagePath } = createdScheduledTask;

        const response = await request(server)
          .get('/api/scheduled-tasks')
          .set('Authorization', `Bearer ${token}`)
          .expect(OK)
          .end()
          .get('body');

        response.should.have.property('tasks')
          .which.is.an.Array()
          .and.have.matchEach(function (responseTask) {
            responseTask.should.have.value('id', id);
            responseTask.should.have.value('title', title);
            responseTask.should.have.value('imagePath', taskDto.composeImageUrl(imagePath));
            responseTask.should.have.value('points', points);
            responseTask.should.have.property('schedule')
              .which.is.Array().and.have.matchEach(function (schedule) {
                schedule.should.have.value('scheduleTaskId', scheduleTaskId);
                schedule.should.have.value('status', 'done');
              });
          });
      });
    });
  });
});
