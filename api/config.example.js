module.exports = {
  port: 3000,
  staticBasePath: 'http://localhost:3000/static',

  database: {
    host: '',
    database: '',
    username: '',
    password: '',
    port: 5432,
    dialect: 'postgres',
    logging: false,
  },

  swagger: {
    host: 'localhost:3000',
    schemes: ['http'],
  },

  jwtInfo: {
    settings: {
      algorithm: '',
      expiresIn: Date.now() + 2629743,
    },
    secret: '',
  },

  cors: {
    origin: '',
  },
};
