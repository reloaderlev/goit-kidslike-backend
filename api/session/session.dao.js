import { Model } from 'sequelize';
import { sessionStatus } from './session.model';

export class Session extends Model {
  static createSession(userId) {
    return this.create({ userId, status: sessionStatus.ACTIVE });
  }

  static findSessionById(id) {
    return this.findByPk(id);
  }

  static disableSession(id) {
    return this.update({ status: sessionStatus.DISABLED }, { where: { id }, returning: true, raw: true });
  }
}
