import { STRING, INTEGER } from 'sequelize';
import { Session } from './session.dao.js';

export const sessionStatus = {
  ACTIVE: 'active',
  DISABLED: 'disabled',
};

export const initSession = Session.init.bind(Session, {
  id: {
    type: INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },

  userId: {
    type: INTEGER,
    allowNull: false,
  },

  status: {
    type: STRING,
    enum: [sessionStatus.ACTIVE, sessionStatus.DISABLED],
    allowNull: false,
  },
});
