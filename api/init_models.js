import { initExample } from './example/example.model';
import { initUser } from './user/user.model';
import { initSession } from './session/session.model';
import { initTask } from './task/task.model';
import { initScheduledTask } from './scheduledTask/scheduledTask.model';
import { initGift } from './gift/gift.model';
import { createTasks } from './helpers/tasksCreator';
import tasks from './src/defaulttasks.json';
import { Task } from './task/task.dao';
import { ScheduledTask } from './scheduledTask/scheduledTask.dao';

export async function initModels() {
  const sequelize = global.sequelize;

  initExample({ sequelize });
  initUser({ sequelize, alter: { drop: false }, modelName: 'Users' });
  initSession({ sequelize, modelName: 'Session' });
  initTask({ sequelize, modelName: 'Task' });
  initScheduledTask({
    sequelize,
    alter: { drop: false },
    modelName: 'ScheduledTask',
  });
  initGift({ sequelize, alter: { drop: false }, modelName: 'Gift' });

  Task.hasMany(ScheduledTask, {
    foreignKey: 'taskId',
    foreignKeyConstraint: true,
    as: 'schedule',
  });

  await createTasks(tasks);

  await sequelize.sync();
}
