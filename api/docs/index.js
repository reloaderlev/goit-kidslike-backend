import { swagger } from '../config';
import { exampleEndpoints, exampleDefinitions } from './example.docs';
import { scheduledTaskEndpoints, scheduledTaskDefinitions } from './scheduledTask.docs';
import { taskEndpoints, taskDefinitions } from './task.docs';
import { userEndpoints, userDefinitions } from './user.docs';
import { authEndpoints, authDefinitions } from './auth.docs';
import { giftEndpoints, giftDefinitions } from './gift.docs';

export default {
  swagger: '2.0',
  info: {
    description: '',
    version: '1.0.0',
    title: 'Wallet Docs',
    contact: {
      email: 'mykola.levkiv@gmail.com',
    },
  },
  host: swagger.host,
  basePath: '/api',
  tags: [],
  schemes: swagger.schemes,
  paths: {
    ...exampleEndpoints,
    ...authEndpoints,
    ...scheduledTaskEndpoints,
    ...taskEndpoints,
    ...userEndpoints,
    ...giftEndpoints,
  },
  definitions: {
    ...exampleDefinitions,
    ...authDefinitions,
    ...scheduledTaskDefinitions,
    ...taskDefinitions,
    ...userDefinitions,
    ...giftDefinitions,
  },
};
