export const giftEndpoints = {
  '/gift': {
    get: {
      tags: ['gift'],
      summary: 'Get affordable gifts',
      description: '',
      operationId: 'getGift',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
      ],
      responses: {
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '200': {
          description: 'When available gifts are received',
          schema: {
            $ref: '#/definitions/GiftsArray',
          },
        },
      },
    },
  },
  '/gift/{id}': {
    post: {
      tags: ['gift'],
      summary: 'Purchase gift',
      description: '',
      operationId: 'getGift',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'path',
          name: 'id',
          description: 'Gift id',
          type: 'string',
          required: true,
        },
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
      ],
      responses: {
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '403': {
          description: 'If user balance is too small for gift purchase',
        },
        '404': {
          description: 'If gift with such id does not exist',
        },
        '201': {
          description: 'If gift had purchased',
          schema: {
            $ref: '#/definitions/UserBalance',
          },
        },
      },
    },
  },
};

export const giftDefinitions = {
  GiftsArray: {
    type: 'object',
    properties: {
      gifts: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'number',
              format: 'int32',
            },
            title: {
              type: 'string',
            },
            cost: {
              type: 'number',
              format: 'int32',
            },
            imagePath: {
              type: 'string',
            },
          },
        },
      },
    },
    xml: {
      name: 'giftsArray',
    },
  },
  UserBalance: {
    type: 'object',
    properties: {
      balance: {
        type: 'number',
        format: 'int32',
      },
    },
  },
};
