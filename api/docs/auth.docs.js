export const authEndpoints = {
  '/auth/signup': {
    post: {
      tags: ['auth'],
      summary: 'Signup and create new user in database',
      description: '',
      operationId: 'getUser',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'body',
          name: 'signup',
          description: 'User object that needs to be created',
          required: true,
          schema: {
            $ref: '#/definitions/SignupRequest',
          },
        },
      ],
      responses: {
        '400': {
          description: 'Request body format is invalid',
        },
        '409': {
          description: 'User with such email already exists',
        },
        '201': {
          description: 'User and session were successfully created',
          schema: {
            $ref: '#/definitions/Signup',
          },
        },
      },
    },
  },

  '/auth/signout': {
    delete: {
      tags: ['auth'],
      summary: 'Disable session',
      description: '',
      operationId: 'getSignOut',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
      ],
      responses: {
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '204': {
          description: 'When user session disabled',
        },
      },
    },
  },
};

export const authDefinitions = {
  SignupRequest: {
    type: 'object',
    properties: {
      email: {
        type: 'string',
      },
      password: {
        type: 'string',
      },
      name: {
        type: 'string',
      },
    },
    xml: {
      name: 'Signup',
    },
  },
  Signup: {
    type: 'object',
    properties: {
      user: {
        type: 'object',
        properties: {
          id: {
            type: 'number',
            format: 'int32',
          },
          name: {
            type: 'string',
          },
          email: {
            type: 'string',
          },
          password: {
            type: 'string',
          },
          avatar: {
            type: 'string',
          },
          balance: {
            type: 'number',
            format: 'int32',
          },
        },
      },
      token: {
        type: 'string',
      },
    },
    xml: {
      name: 'Signup',
    },
  },
};
