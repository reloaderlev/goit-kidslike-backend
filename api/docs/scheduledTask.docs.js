export const scheduledTaskEndpoints = {
  '/scheduled-tasks': {
    post: {
      tags: ['scheduledTask'],
      summary: 'Change status for schedule tasks',
      description: '',
      operationId: 'getScheduleTask',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
        {
          in: 'body',
          name: 'scheduleTask',
          description: 'scheduled task which must be changed ',
          required: true,
          schema: {
            $ref: '#/definitions/ScheduleTaskRequest',
          },
        },
      ],
      responses: {
        '400': {
          description: 'Request body format is invalid',
        },
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '403': {
          description: 'When trying to schedule task which is not general and user not author of it',
        },
        '404': {
          description: 'When trying to change task which not exist',
        },
        '200': {
          description: 'When scheduled tasks updated',
          schema: {
            $ref: '#/definitions/ScheduledTasksArray',
          },
        },
      },
    },
    patch: {
      tags: ['scheduledTask'],
      summary: 'Togle scheduled tasks',
      description: '',
      operationId: 'getScheduleTask',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
        {
          in: 'body',
          name: 'toggledScheduledTask',
          description: 'scheduled task which must be changed',
          required: true,
          schema: {
            $ref: '#/definitions/ToggledScheduledTaskRequest',
          },
        },
      ],
      responses: {
        '400': {
          description: 'Request body format is invalid',
        },
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '403': {
          description: 'When trying to toggle task and user not owner of it',
        },
        '200': {
          description: 'When scheduled tasks updated',
          schema: {
            $ref: '#/definitions/ToggledScheduledTaskArray',
          },
        },
      },
    },
    get: {
      tags: ['scheduledTask'],
      summary: 'Get all scheduled tasks',
      description: '',
      operationId: 'getScheduledTask',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
      ],
      responses: {
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '200': {
          description: 'When scheduled tasks recived',
          schema: {
            $ref: '#/definitions/UsersScheduledTasksArray',
          },
        },
      },
    },
  },
};

export const scheduledTaskDefinitions = {
  ScheduleTaskRequest: {
    type: 'object',
    properties: {
      taskId: {
        type: 'number',
        format: 'int32',
      },
      days: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            date: {
              type: 'string',
              format: 'date-time',
            },
            schedule: {
              type: 'boolean',
            },
          },
        },
      },

    },
    xml: {
      name: 'ScheduledTask',
    },
  },

  ScheduledTasksArray: {
    type: 'object',
    properties: {
      tasks: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'number',
              format: 'int32',
            },
            schedule: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  scheduleTaskId: {
                    type: 'number',
                    format: 'init32',
                  },
                  dueDate: {
                    type: 'string',
                    format: 'date-time',
                  },
                  status: {
                    type: 'string',
                  },
                },
              },
            },
            title: {
              type: 'string',
            },
            imagePath: {
              type: 'string',
            },
            points: {
              type: 'number',
              format: 'int32',
            },
          },
        },
      },
    },
    xml: {
      name: 'ScheduledTaskResponse',
    },
  },

  ToggledScheduledTaskRequest: {
    type: 'array',
    items: {
      type: 'object',
      properties: {
        scheduleTaskId: {
          type: 'number',
          format: 'int32',
        },
        status: {
          type: 'string',
          enum: ['scheduled', 'done'],
        },
      },
    },
    xml: {
      name: 'ToggledScheduledTask',
    },
  },

  ToggledScheduledTaskArray: {
    type: 'object',
    properties: {
      tasks: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'number',
              format: 'int32',
            },
            schedule: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  scheduleTaskId: {
                    type: 'number',
                    format: 'int32',
                  },
                  dueDate: {
                    type: 'string',
                    format: 'date-time',
                  },
                  status: {
                    type: 'string',
                  },
                },
              },
            },
            title: {
              type: 'string',
            },
            imagePath: {
              type: 'string',
            },
            points: {
              type: 'number',
              format: 'int32',
            },
          },
        },
      },
    },
    xml: {
      name: 'ToggledScheduledTaskArray',
    },
  },

  UsersScheduledTasksArray: {
    type: 'object',
    properties: {
      tasks: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'number',
              format: 'int32',
            },
            schedule: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  scheduleTaskId: {
                    type: 'number',
                    format: 'int32',
                  },
                  dueDate: {
                    type: 'string',
                    format: 'date-time',
                  },
                  status: {
                    type: 'string',
                  },
                },
              },
            },
            title: {
              type: 'string',
            },
            imagePath: {
              type: 'string',
            },
            points: {
              type: 'number',
              format: 'int32',
            },
          },
        },
      },
    },
    xml: {
      name: 'UsersScheduledTasksArray',
    },
  },
};
