export const userEndpoints = {
  '/users/current': {
    get: {
      tags: ['user'],
      summary: 'Get user info',
      description: '',
      operationId: 'getUser',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
      ],
      responses: {
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '200': {
          description: 'When user information is received',
          schema: {
            $ref: '#/definitions/UserInfo',
          },
        },
      },
    },
  },
};

export const userDefinitions = {
  UserInfo: {
    type: 'object',
    properties: {
      user: {
        type: 'object',
        properties: {
          id: {
            type: 'number',
            format: 'init32',
          },
          name: {
            type: 'string',
            nullable: true,
          },
          email: {
            type: 'string',
          },
          avatar: {
            type: 'string',
          },
          balance: {
            type: 'number',
            format: 'init32',
          },
        },
      },
    },
    xml: {
      name: 'taskArray',
    },
  },
};
