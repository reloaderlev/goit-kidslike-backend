export const taskEndpoints = {
  '/task': {
    post: {
      tags: ['task'],
      summary: 'Create custom user task',
      description: '',
      operationId: 'getTask',
      consumes: ['application/json'],
      produces: ['application/json'],
      parameters: [
        {
          in: 'header',
          name: 'Authorization',
          type: 'string',
          required: true,
        },
        {
          in: 'body',
          name: 'task',
          description: 'custom task which must be created ',
          required: true,
          schema: {
            $ref: '#/definitions/TaskRequest',
          },
        },
      ],
      responses: {
        '400': {
          description: 'Request body format is invalid',
        },
        '401': {
          description: 'Unauthorized user, user token is invalid',
        },
        '409': {
          description: 'When task with sended title already exist for this user',
        },
        '201': {
          description: 'When custom task created',
          schema: {
            $ref: '#/definitions/TasksArray',
          },
        },
      },
    },
  },
};

export const taskDefinitions = {
  TaskRequest: {
    type: 'object',
    properties: {
      title: {
        type: 'string',
      },
      taskPoints: {
        type: 'number',
        format: 'int32',
      },
    },
    xml: {
      name: 'task',
    },
  },

  TasksArray: {
    type: 'object',
    properties: {
      tasks: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'number',
              format: 'int32',
            },
            schedule: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  scheduleTaskId: {
                    type: 'number',
                    format: 'init32',
                  },
                  dueDate: {
                    type: 'string',
                    format: 'date-time',
                  },
                  status: {
                    type: 'string',
                    default: 'cancelled',
                  },
                },
              },
            },
            title: {
              type: 'string',
            },
            imagePath: {
              type: 'string',
            },
            points: {
              type: 'number',
              format: 'int32',
            },
          },
        },
      },
    },
    xml: {
      name: 'taskArray',
    },
  },
};
