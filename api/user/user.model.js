import { STRING, INTEGER } from 'sequelize';
import { User } from './user.dao';

export const initUser = User.init.bind(User, {
  id: { type: INTEGER, primaryKey: true, autoIncrement: true },
  name: { type: STRING },
  email: { type: STRING, allowNull: false, unique: true },
  passwordHash: { type: STRING },
  avatar: { type: STRING, allowNull: true },
  balance: { type: INTEGER, allowNull: false },
});
