import { OK } from 'http-status-codes';
import { userDto } from './user.dto';
import { User } from './user.dao';
import { scheduledTaskStatus } from '../scheduledTask/scheduledTask.dao';

export class UserController {

  static async changeUserBalance(scheduledTasks) {

    for (let scheduledTask of scheduledTasks) {
      const { ownerId, points, status, previousStatus } = scheduledTask;
      const value = status === previousStatus || previousStatus === scheduledTaskStatus.NOTSCHEDULED ?
        0 :
        status === scheduledTaskStatus.DONE ? points : - points;

      await User.changeUserBalance(value, ownerId);
    }
  }

  get getUser() {
    return this._getUser.bind(this);
  }

  async _getUser(req, res) {
    const { user } = req.userSession;
    const responseBody = userDto.comproseUserLoddedIn(user);

    return res
      .status(OK)
      .send(responseBody)
      .end();
  }
}
