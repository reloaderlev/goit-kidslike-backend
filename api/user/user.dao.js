import { Model, literal } from 'sequelize';
import bcrypt from 'bcryptjs';

export class User extends Model {
  static async createUser(email, passwordHash, name) {
    return this.findOrCreate({
      where: { email },
      defaults: { passwordHash, name: name || null, avatar: '', balance: 0 },
    });
  }

  static async findUserById(id) {
    return this.findByPk(id);
  }

  static findUserByEmail(email) {
    return this.findOne({ where: { email } });
  }

  static async changeUserBalance(value, id) {
    return this.update(
      { balance: literal(`balance + ${value}`) },
      { where: { id }, returning: true },
    );
  }

  async isValidPassword(password) {
    return bcrypt.compare(password, this.passwordHash);
  }
}
