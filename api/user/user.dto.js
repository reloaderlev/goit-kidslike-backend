class UserDto {
  composeUserSignUpResponse(user, token) {
    const userData = this.comproseUserLoddedIn(user);

    return {
      ...userData,
      token,
    };
  }

  comproseUserLoddedIn(user) {
    const { id, name, email, avatar, balance } = user;

    return {
      user: {
        id,
        name,
        email,
        avatar,
        balance,
      },
    };
  }

  composeUserBalance(user) {
    const { balance } = user;

    return { balance };
  }
}

export const userDto = new UserDto();
