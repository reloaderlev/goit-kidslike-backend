// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { KidslikeServer } = require('../server');
const request = require('supertest-promised');
const { UNAUTHORIZED, OK } = require('http-status-codes');
const { User } = require('../user/user.dao');
const { AuthController } = require('../auth/auth.controller');

describe('User logged API', () => {
  let server;
  let kidslikeServer;
  let token;
  let createdUser;
  const user = {
    email: 'loggeduser@user.com.ca',
    password: 'password',
    name: 'LoggedUser',
  };
  const authController = new AuthController();

  before(async () => {
    kidslikeServer = new KidslikeServer();
    await kidslikeServer.start();
    await kidslikeServer.truncateModels(User);
    server = kidslikeServer.server;
    const { email, password, name } = user;
    const [{ dataValues: userInstance }] = await User.createUser(email, password, name);
    createdUser = userInstance;
    token = await authController.createSessionToken(userInstance.id);
  });

  after(async () => {
    await kidslikeServer.truncateModels(User);
    server.close();
  });

  describe('GET /users/current', () => {

    context('when use invalid token', () => {

      it('should throw 401 error', async () => {
        await request(server)
          .get('/api/users/current')
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when use valid token', () => {

      it('should be statuss OK(200)', async () => {

        const { id, name, email, avatar, balance } = createdUser;

        const response = await request(server)
          .get('/api/users/current')
          .set('Authorization', `Bearer ${token}`)
          .expect(OK)
          .end()
          .get('body');

        response.should.be.an.Object()
          .which.have.matchEach((resUser) => {
            resUser.should.have.value('id', id);
            resUser.should.have.value('name', name);
            resUser.should.have.value('email', email);
            resUser.should.have.value('avatar', avatar);
            resUser.should.have.value('balance', balance);
          });
      });
    });
  });
});
