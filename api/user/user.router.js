import { Router } from 'express';
import { authMiddleware } from '../helpers/auth.middleware';
import { asyncWrapper } from '../helpers/asyncMiddlewareWrapper';
import { UserController } from './user.controller';

class UserRouter {
  static initRouter() {
    const userController = new UserController();
    const router = Router();

    router.get('/', authMiddleware, asyncWrapper(userController.getUser));

    return router;
  }
}

export const userRouter = UserRouter.initRouter();
