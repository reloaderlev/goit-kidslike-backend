import express from 'express';
import bodyParser from 'body-parser';
import config from './config';
import path from 'path';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './docs/index';
import Sequelize from 'sequelize';
import morgan from 'morgan';
import cors from 'cors';
import { initModels } from './init_models';
import { exampleRouter } from './example/example.router';
import { authRouter } from './auth/auth.router';
import { scheduledTaskRouter } from './scheduledTask/scheduledTask.router';
import { taskRouter } from './task/task.router';
import { userRouter } from './user/user.router';
import { giftRouter } from './gift/gift.router';
import { PassportStrategies } from './auth/passport.strategies';

export class KidslikeServer {
  constructor() {
    this.app = express();
    this.config = config;
  }

  async start() {
    PassportStrategies.initLocalStrategy();
    this.initMiddlewares();
    await this.initDb();
    this.initRoutes();

    this.startListening();
  }

  initMiddlewares() {
    this.app.use(morgan());
    this.app.use(cors(this.config.cors));
    this.app.use(bodyParser.json());
    this.app.use(
      '/static',
      express.static(path.join(__dirname, '/src/images')),
    );
  }

  initRoutes() {
    this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    this.app.use('/api/examples', exampleRouter);
    this.app.use('/api/auth', authRouter);
    this.app.use('/api/scheduled-tasks', scheduledTaskRouter);
    this.app.use('/api/tasks', taskRouter);
    this.app.use('/api/users/current', userRouter);
    this.app.use('/api/gift', giftRouter);
  }

  async initDb() {
    try {
      const sequelize = new Sequelize(this.config.database);
      await sequelize.authenticate();

      global.sequelize = sequelize;
      await initModels();

      console.log('Database started successfully');
    } catch (err) {
      console.log('Database failure', err);
    }
  }

  async truncateModels(...models) {
    await Promise.all(
      models.map((model) => {
        return model.truncate({ cascade: true });
      }),
    );
  }

  startListening() {
    this.server = this.app.listen(this.config.port, () => {
      console.log('Server started on port', this.config.port);
    });
  }
}
