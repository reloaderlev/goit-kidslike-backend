import { Task } from '../task/task.dao';
import { scheduledTaskDto } from '../scheduledTask/scheduledTask.dto';
import { ScheduledTask } from '../scheduledTask/scheduledTask.dao';
import { setActualDates } from './dateHelper';


export async function createTasks(tasks, authorId = null) {

  for (let task of tasks) {
    const { title, points, imagePath } = task;
    await Task.initDefaultTask(title, points, imagePath, authorId);
  }

  const allTasks = await Task.getAllTasks();

  return allTasks;
}

export async function createScheduledTasks(tasks, user) {
  const days = setActualDates();

  for (let task of tasks) {
    const { dataValues: taskInfo } = task;
    for (let date of days) {
      const data = scheduledTaskDto.composeDataForScheduleTask(user, taskInfo, date);
      await ScheduledTask.createScheduledTask(...data);
    }
  }
}
