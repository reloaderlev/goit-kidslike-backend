import { CryptoHelper } from './cryptoHelper';
import { Session } from '../session/session.dao';
import { sessionStatus } from '../session/session.model';
import { User } from '../user/user.dao';
import { UnauthorizedError } from './errorsConstructors';

export async function authMiddleware(req, res, next) {
  try {
    const sessionToken = req.headers.authorization.replace('Bearer ', '');
    const { sessionID } = await CryptoHelper.checkSessionToken(sessionToken);

    const isSession = await Session.findSessionById(sessionID);

    if (!isSession) throw new UnauthorizedError('Session not found!');

    const { dataValues: session } = isSession;

    if (session.status === sessionStatus.DISABLED) throw new UnauthorizedError('Session disabled!');

    const { dataValues: user } = await User.findUserById(session.userId);

    req.userSession = { session, user };
    next();
  } catch (err) {
    next(err);
  }
}
