import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { jwtInfo } from '../config';
import { UnauthorizedError } from './errorsConstructors';


export class CryptoHelper {

  static async setPasswordHash(password) {
    const salt = await bcrypt.genSalt(10);

    return bcrypt.hash(password, salt);
  }

  static async checkUserPassword(password, passwordHash) {
    return bcrypt.compare(password, passwordHash);
  }

  static async setSessionToken(sessionID) {
    return jwt.sign({ sessionID }, jwtInfo.secret, jwtInfo.settings);
  }

  static async checkSessionToken(token) {
    try {
      return jwt.verify(token, jwtInfo.secret);
    } catch (err) {
      throw new UnauthorizedError('Invalid token');
    }
  }
}
