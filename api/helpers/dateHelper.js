class CustomDate extends Date {
  constructor(...args) {
    super(...args);
    this.setUTCHours(0);
  }

  getWeek() {
    const date = new Date(this.getTime());
    date.setHours(0, 0, 0, 0);
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    const week1 = new Date(date.getFullYear(), 0, 4);
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
      - 3 + (week1.getDay() + 6) % 7) / 7);
  }
}

export const dateInterval = {
  currentWeek: [7, 1],
  beforeCurrentLast: [14, -6],
};

export function fullDate(bias = 0) {
  const year = new CustomDate().getFullYear();
  const month = new CustomDate().getMonth();
  const date = new CustomDate().getDate();

  return new CustomDate(year, month, date + bias);
}

export function weekNow() {
  return new CustomDate(fullDate()).getWeek();
}

export function isDayOfCurrentWeek(date) {
  return weekNow() === new CustomDate(date).getWeek();
}

export function setActualDates(max, min) {
  const dateNow = fullDate();
  const weekDay = new CustomDate(dateNow).getDay() ? new CustomDate(dateNow).getDay() : 7;
  const daysArr = [];

  for (let i = max; i >= min; i--) {
    const date = fullDate(i - weekDay);

    daysArr.push(date);
  }

  return daysArr;
}
