import Validator from 'node-validator';
import { AbstractValidator } from '../helpers/AbstractValidator';

export class AuthValidator extends AbstractValidator {
  constructor() {
    super();
  }

  get signUpUser() {
    return this._signUpUser.bind(this)();
  }

  _signUpUser() {
    const signUpUserRules = Validator.isObject()
      .withOptional('name', Validator.isString())
      .withRequired('email', Validator.isString())
      .withRequired('password', Validator.isString({
        regex: /[\w*|\D*]{6,12}/,
        message: 'Password must to consist more than 5 simbols and less than 13 simbols!',
      }));

    return this.runMiddleware.bind(this, signUpUserRules, 'body');
  }
}
