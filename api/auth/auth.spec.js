// eslint-disable-next-line no-global-assign
require = require('esm')(module);
require('should');
const { KidslikeServer } = require('../server');
const request = require('supertest-promised');
const { UNAUTHORIZED, NO_CONTENT, BAD_REQUEST, CONFLICT, CREATED } = require('http-status-codes');
const { User } = require('../user/user.dao');
const { Session } = require('../session/session.dao');
const { AuthController } = require('../auth/auth.controller');

describe('Auth API', () => {
  let server;
  let kidslikeServer;
  let token;
  const authController = new AuthController();
  const user = {
    email: 'logoutUser@user.com.ca',
    password: 'password',
    name: 'LogoutUser',
  };

  before(async () => {
    kidslikeServer = new KidslikeServer();
    await kidslikeServer.start();
    await kidslikeServer.truncateModels(User, Session);
    server = kidslikeServer.server;
  });

  after(async () => {
    await kidslikeServer.truncateModels(User, Session);
    server.close();
  });

  describe('POST /auth/signup', () => {

    context('when invalid request body', () => {
      it('should throw 400 error', async () => {

        const userWithInvalidEmail = {
          email: 1236,
          password: 'password',
        };

        await request(server)
          .post('/api/auth/signup')
          .send(userWithInvalidEmail)
          .set('Accept', 'application/json')
          .expect(BAD_REQUEST)
          .end();

      });
    });

    it('should throw 400 error', async () => {

      const userWithSmallPassword = {
        email: 'user@user.email',
        password: 'pas',
      };

      await request(server)
        .post('/api/auth/signup')
        .send(userWithSmallPassword)
        .set('Accept', 'application/json')
        .expect(BAD_REQUEST)
        .end();
    });
  });

  context('when creat a user', () => {

    it('should create user', async () => {

      const responseUser = await request(server)
        .post('/api/auth/signup')
        .send(user)
        .set('Accept', 'application/json')
        .expect(CREATED)
        .end()
        .get('body');

      responseUser.should.have.property('token').which.is.a.String();
      responseUser.should.have.property('user')
        .which.is.an.Object()
        .have.properties('id', 'name', 'avatar', 'balance')
        .and.have.value('email', user.email);
    });

    after(async () => {
      await kidslikeServer.truncateModels(User);
    });

  });

  context('when trying to create user with reserved email', () => {

    before(async () => {
      const { email, password, name } = user;
      await User.createUser(email, password, name);
    });

    after(async () => {
      await kidslikeServer.truncateModels(User);
    });

    it('should throw 409 error', async () => {
      await request(server)
        .post('/api/auth/signup')
        .send(user)
        .set('Accept', 'application/json')
        .expect(CONFLICT)
        .end();
    });
  });

  describe('DELETE /auth/signout', () => {

    before(async () => {
      const { email, password, name } = user;
      const [{ dataValues: userInstance }] = await User.createUser(email, password, name);
      token = await authController.createSessionToken(userInstance.id);
    });

    context('when use invalid token', () => {

      it('should throw 401 error', async () => {

        await request(server)
          .delete('/api/auth/signout')
          .set('Authorization', `Bearer ${token + 'A'}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });

    context('when session disabled', () => {

      it('should be status NO CONTENT(204)', async () => {

        await request(server)
          .delete('/api/auth/signout')
          .set('Authorization', `Bearer ${token}`)
          .expect(NO_CONTENT)
          .end();
      });

      it('should throw 401 error', async () => {

        await request(server)
          .delete('/api/auth/signout')
          .set('Authorization', `Bearer ${token}`)
          .expect(UNAUTHORIZED)
          .end();
      });
    });
  });
});
