import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { User } from '../user/user.dao';

export class PassportStrategies {
  static initLocalStrategy() {
    passport.use(
      new LocalStrategy(
        {
          usernameField: 'email',
          passwordField: 'password',
        },
        async (email, password, done) => {
          const user = await User.findUserByEmail(email);

          if (!user) {
            return done(null, false, { message: 'Incorrect email' });
          }

          const validPassword = await user.isValidPassword(password);

          if (!validPassword) {
            return done(null, false, { message: 'Incorrect password' });
          }

          return done(null, user);
        },
      ),
    );
  }
}
