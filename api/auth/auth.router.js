import { Router } from 'express';
import passport from 'passport';
import { AuthValidator } from './auth.validator';
import { AuthController } from './auth.controller';
import { authMiddleware } from '../helpers/auth.middleware';
import { asyncWrapper } from '../helpers/asyncMiddlewareWrapper';

class AuthRouter {
  static initRouter() {
    const authValidator = new AuthValidator();
    const authController = new AuthController();
    const router = Router();

    router.post(
      '/signup',
      authValidator.signUpUser,
      asyncWrapper(authController.signUpUser),
    );
    router.post(
      '/signin',
      passport.authenticate('local', { session: false }),
      asyncWrapper(authController.createSession),
    );
    router.delete(
      '/signout',
      authMiddleware,
      asyncWrapper(authController.logoutUser),
    );

    return router;
  }
}

export const authRouter = AuthRouter.initRouter();
