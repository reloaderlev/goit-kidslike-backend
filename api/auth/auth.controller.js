import { CREATED, NO_CONTENT } from 'http-status-codes';
import { Session } from '../session/session.dao';
import { User } from '../user/user.dao';
import { userDto } from '../user/user.dto';
import { CryptoHelper } from '../helpers/cryptoHelper';
import { ConflictError } from '../helpers/errorsConstructors';

export class AuthController {
  get signUpUser() {
    return this._signUpUser.bind(this);
  }

  get createUserInDb() {
    return this._createUserInDb.bind(this);
  }

  get createSessionToken() {
    return this._createSessionToken.bind(this);
  }

  get logoutUser() {
    return this._logoutUser.bind(this);
  }

  async _signUpUser(req, res) {
    const { email, password, name } = req.body;
    console.log('existing user', await User.findAll({ where: { email } }));
    const [{ dataValues: user }, status] = await this._createUserInDb(
      email,
      password,
      name,
    );

    if (!status) throw new ConflictError(`User with ${email} already exists`);

    const { id } = user;
    const token = await this._createSessionToken(id);
    const resBody = await userDto.composeUserSignUpResponse(user, token);

    return res.status(CREATED).json(resBody);
  }

  async _createUserInDb(email, password, name) {
    const passwordHash = await CryptoHelper.setPasswordHash(password);
    return User.createUser(email, passwordHash, name);
  }

  get createSession() {
    return this._createSession.bind(this);
  }

  async _createSession(req, res) {
    const user = req.user;
    const token = await this.createSessionToken(user.id);

    return res
      .status(CREATED)
      .json(userDto.composeUserSignUpResponse(user, token));
  }

  async _createSessionToken(userId) {
    const {
      dataValues: { id: sessionID },
    } = await Session.createSession(userId);
    return CryptoHelper.setSessionToken(sessionID);
  }

  async _logoutUser(req, res) {
    const { session } = req.userSession;

    await Session.disableSession(session.id);

    return res.status(NO_CONTENT).end();
  }
}
