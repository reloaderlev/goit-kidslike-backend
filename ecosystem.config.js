module.exports = {
  apps: [
    {
      name: 'protest-server',
      script: './api/start.js',
    },
  ],
};
